import { useState, useEffect } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";

export default function AddCards() {
    const [isBtnDisabled, setIsbtnDisabled] = useState(true);

    /* Card useState */
    const [cardName, setCardName] = useState("");
    const [description, setDescription] = useState("");
    const [gameName, setGameName] = useState("");
    const [price, setPrice] = useState("");
    const [stock, setStock] = useState(0);
    const [isActive, setIsActive] = useState(true);
    const [imgSrc, setImgSrc] = useState("");

    useEffect(() => {
        clearInputs();
    }, []);

    useEffect(() => {
        if (cardName && description && gameName && price && stock && imgSrc) {
            setIsbtnDisabled(false);
        } else {
            setIsbtnDisabled(true);
        }
    }, [cardName, description, gameName, price, stock, imgSrc]);

    function addCard(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_URI}/products/cards/add`, {
            method: `POST`,
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: JSON.stringify({
                cardName: cardName,
                description: description,
                gameName: gameName,
                price: price,
                stock: stock,
                isActive: isActive,
                imgSrc: imgSrc,
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.isCardAdded) {
                    alert(`Card "${cardName}" has been added successfully `);
                    clearInputs();
                } else {
                    alert(
                        `There was an error during the process. Please try again.`
                    );
                }
            });
    }

    function clearInputs() {
        setCardName("");
        setDescription("");
        setGameName("");
        setPrice("");
        setStock("");
        setImgSrc("");
    }

    return (
        <Row>
            <Col
                xs={4}
                sm={6}
                md={8}
                className="offset-4 offset-sm-3 offset-md-2"
            >
                <Form className="text-dark bg-light p-3" onSubmit={addCard}>
                    <Form.Group className="mb-3" controlId="cardName">
                        <Form.Label>Card Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter the card name"
                            value={cardName}
                            onChange={(event) => {
                                setCardName(event.target.value);
                            }}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control
                            type="text"
                            as="textarea"
                            placeholder="Enter the description"
                            value={description}
                            onChange={(event) => {
                                setDescription(event.target.value);
                            }}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="gameName">
                        <Form.Label>Game:</Form.Label>
                        <Form.Select
                            defaultValue={""}
                            onChange={(event) => {
                                setGameName(event.target.value);
                            }}
                            required
                        >
                            <option disabled value="">
                                Select game
                            </option>
                            <option value="Yu-Gi-Oh!">Yu-Gi-Oh!</option>
                            <option value="Magic - The Gathering">
                                Magic - The Gathering
                            </option>
                        </Form.Select>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="price">
                        <Form.Label>Price:</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="Enter the price"
                            value={price}
                            onChange={(event) => {
                                setPrice(event.target.value);
                            }}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="stock">
                        <Form.Label>Stock:</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="Enter the stock"
                            value={stock}
                            onChange={(event) => {
                                setStock(event.target.value);
                            }}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="imgSrc">
                        <Form.Label>Image:</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter the image URL"
                            value={imgSrc}
                            onChange={(event) => {
                                setImgSrc(event.target.value);
                            }}
                            required
                        />
                    </Form.Group>

                    <Button
                        variant="primary"
                        type="submit"
                        disabled={isBtnDisabled}
                    >
                        Add Card
                    </Button>
                </Form>
            </Col>
        </Row>
    );
}
