import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useEffect } from "react";

export default function CheckOut() {
    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/order/checkout`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.isCartEmpty) {
                    console.log("Checked-out successfully");
                }
            });
    }, []);

    return (
        <Container>
            <h1>Checked-out successfully</h1>
            <p>
                Go back to <Link to="/home">homepage</Link>.
            </p>
        </Container>
    );
}
