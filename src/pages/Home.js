import Banner from "../components/Banner.js";
import Highlights from "../components/Highlights.js";

export default function Home() {
    return (
        <div className="d-flex flex-column m-0 align-items-center">
            <Banner />
            <Highlights />
        </div>
    );
}
