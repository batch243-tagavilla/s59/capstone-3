import { Fragment, useState, useEffect } from "react";
import CardContainer from "../components/CardContainer";

export default function ActiveCardsList() {
    const [activeCards, setActiveCards] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/products/cards/list/active`)
            .then((response) => response.json())
            .then((cardsList) => {
                setActiveCards(
                    cardsList.map((card) => {
                        return <CardContainer cardProp={card} key={card._id} />;
                    })
                );
            });
    }, []);

    return (
        <Fragment>
            <div className="text-center border">
                <h4>List of cards</h4>
            </div>

            <div className="d-flex flex-wrap">{activeCards}</div>
        </Fragment>
    );
}
