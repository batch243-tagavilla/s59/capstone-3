import { useEffect, useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import OrderContainer from "../components/OrderContainer";

export default function Cart() {
    const [order, setOrder] = useState({
        id: null,
        userId: null,
        products: [],
        totalAmount: 0,
        dateAdded: null,
    });

    const [product, setProduct] = useState([]);

    useEffect(() => {
        setProduct(
            order.products.map((item) => {
                return <OrderContainer orderProps={item} key={item._id} />;
            })
        );
    }, [order]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/cart/get`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
            .then((response) => response.json())
            .then((data) => {
                if (data) {
                    setOrder({
                        id: data._id,
                        userId: data.userId,
                        products: data.products,
                        totalAmount: data.totalAmount,
                        dateAdded: data.dateAdded,
                    });
                }
            });
    }, []);

    return (
        <Container>
            <Row>
                <Col>
                    <Form>
                        <div>{product}</div>

                        <div className="my-3 d-flex justify-content-between">
                            <div className="d-flex align-items-center">
                                <p className="m-0">
                                    Total Price: {order.totalAmount}
                                </p>
                            </div>
                            <div className="d-flex align-items-center">
                                <Button
                                    variant="primary"
                                    as={Link}
                                    to={"/order/checkout"}
                                >
                                    Check-out
                                </Button>
                            </div>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}
