import { Fragment, useState, useEffect } from "react";
import CardContainer from "../components/CardContainer.js";

export default function AllCardsList() {
    const [card, setCard] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/products/cards/list`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
            .then((response) => response.json())
            .then((cardsList) => {
                setCard(
                    cardsList.map((card) => {
                        return <CardContainer cardProp={card} key={card._id} />;
                    })
                );
            });
    }, []);

    return (
        <Fragment>
            {/* Search bar */}
            {/* <Form className="d-flex my-3">
                        <Form.Control
                            type="search"
                            placeholder="Search"
                            className="me-2"
                            aria-label="Search"
                        />
                        <Button variant="primary">Search</Button>
                    </Form> */}

            <div className="m-3 d-flex flex-column">
                <h4>List of cards</h4>
                <div className="d-flex flex-wrap">{card}</div>
            </div>
        </Fragment>
    );
}
