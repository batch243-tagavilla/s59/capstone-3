import { Fragment, useContext } from "react";
import Dashboard from "../components/Dashboard";
import UserContext from "../context/UserContext.js";

export default function Profile() {
    const { user } = useContext(UserContext);

    return (
        <div className="d-flex flex-column m-0 p-5 bg-light">
            {user.id && user.isAdmin ? (
                <Fragment>
                    <h1>Admin Dashboard</h1>
                    <Dashboard />
                </Fragment>
            ) : (
                <Fragment>
                    <h1>Profile</h1>
                    <Dashboard />
                </Fragment>
            )}
        </div>
    );
}
