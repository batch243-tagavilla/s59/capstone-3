import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function NotFound() {
    return (
        <Container>
            <h1>Page Not Found</h1>
            <p>
                Go back to <Link to="/home">homepage</Link>.
            </p>
        </Container>
    );
}
