import { useEffect, useState, useContext } from "react";
import { Row, Col, Container, Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../context/UserContext.js";

export default function Login() {
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [isDisabled, setIsDisabled] = useState(true);

    const { setUser } = useContext(UserContext);

    useEffect(() => {
        if ((email && password) || (username && password)) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password]);

    function loginUser(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_URI}/account/login`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                email: email,
                username: username,
                password: password,
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.accessToken) {
                    localStorage.setItem("token", data.accessToken);
                    retrieveUserDetails(data.accessToken);
                    alert("Login Successful");
                } else {
                    alert("Authentication Failed");
                    setPassword("");
                }
            });

        const retrieveUserDetails = (token) => {
            fetch(`${process.env.REACT_APP_URI}/account/profile`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            })
                .then((response) => response.json())
                .then((data) => {
                    console.log(data);
                    setUser({ id: data._id, username: data.username, isAdmin: data.isAdmin });
                });
        };
    }

    return (
        <Container>
            <Row>
                <Col className="col-md-4 col-8 offset-md-4 offset-2">
                    <Form
                        className="text-dark bg-light p-3"
                        onSubmit={loginUser}
                    >
                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Username / Email address</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter username or email"
                                value={email ? email : username}
                                onChange={(event) => {
                                    if (event.target.value.includes("@")) {
                                        setEmail(event.target.value);
                                        setUsername("");
                                    } else {
                                        setUsername(event.target.value);
                                        setEmail("");
                                    }
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Enter your password"
                                value={password}
                                onChange={(event) => {
                                    setPassword(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Button
                            variant="primary"
                            type="submit"
                            disabled={isDisabled}
                        >
                            Login
                        </Button>

                        <p>Don't have an account yet?</p>
                        <Link to="/account/register">Sign-up here</Link>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}
