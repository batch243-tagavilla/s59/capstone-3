import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../context/UserContext.js";

export default function Logout() {
    const { clearUser } = useContext(UserContext);

    useEffect(() => {
        clearUser();
    }, []);

    return <Navigate to="/account/login" />;
}
