const items = [
    {
        id: "itm001",
        name: "Yu-Gi-Oh!",
        description:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sit deleniti quod vero minima.",
        imgSrc: "https://drive.google.com/uc?export=view&id=1YBdl0sucIMVmqvoomCb544YNZAjccHE9",
        // imgSrc: "https://images.saymedia-content.com/.image/c_limit%2Ccs_srgb%2Cq_auto:eco%2Cw_700/MTg3NDE3NjY0NjgyMDEwMzgz/cards-you-should-play-in-yugioh-master-duel.webp",
        alt: "yugioh-banner",
    },
    {
        id: "itm002",
        name: "Magic - The Gathering",
        description:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus dolorum ab, excepturi quas fugiat numquam quaerat hic veniam.",
        imgSrc: "https://drive.google.com/uc?export=view&id=1V49kAy6lu_8-c8N_GKYxid4iHOr4Idaz",
        alt: "mtg-banner",
    },
];

export default items;
