export default function Footer() {
    return (
        <footer className="bg-dark text-white text-center mt-3 py-3">
            <p className="m-0">All Rights Reserved.</p>
        </footer>
    )
}