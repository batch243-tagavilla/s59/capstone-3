import { useContext, useState, Fragment } from "react";
import { Modal, Form, Card, Button, Col } from "react-bootstrap";
import CardContext from "../context/CardContext.js";
import UserContext from "../context/UserContext.js";

export default function CardView(props) {
    const [showEdit, setShowEdit] = useState(false);
    const { user } = useContext(UserContext);
    const {
        cardId,
        cardName,
        setCardName,
        description,
        setDescription,
        gameName,
        setGameName,
        price,
        setPrice,
        stock,
        setStock,
        isActive,
        setIsActive,
        imgSrc,
        setImgSrc,
    } = useContext(CardContext);

    function updateCard(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_URI}/products/cards/update`, {
            method: `PATCH`,
            headers: {
                "Content-Type": `application/json`,
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: JSON.stringify({
                cardId: cardId,
                cardName: cardName,
                description: description,
                gameName: gameName,
                price: price,
                stock: stock,
                isActive: isActive,
                imgSrc: imgSrc,
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.isCardUpdated) {
                    alert(`Card "${cardName}" has been updated successfully.`);
                    setShowEdit(false);
                } else {
                    alert(
                        `There was an error during the process. Please try again.`
                    );
                }
            });
    }

    const addToCart = () => {
        let quantity = prompt(`How many?`);

        if (quantity) {
            fetch(`${process.env.REACT_APP_URI}/cart/add`, {
                method: `POST`,
                headers: {
                    "Content-Type": `application/json`,
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
                body: JSON.stringify({
                    cardId: cardId,
                    quantity: quantity,
                }),
            })
                .then((response) => response.json())
                .then((data) => {
                    if (data) {
                        alert(
                            `Card "${cardName}" has been added to cart successfully `
                        );
                    } else {
                        alert(
                            `There was an error during the process. Please try again.`
                        );
                    }
                });
        }
    };

    return (
        <Modal
            {...props}
            size="md"
            aria-label="cardViewModal"
            onExit={() => {
                setShowEdit(false);
            }}
            centered
        >
            <Modal.Header closeButton className="text-white bg-secondary">
                <Modal.Title>Card Details</Modal.Title>
            </Modal.Header>
            <Modal.Body className="bg-light">
                <Fragment>
                    {!showEdit ? (
                        <Card.Body className="row p-0">
                            <Col>
                                <img src={imgSrc} className="img-fluid" />
                            </Col>
                            <Col>
                                <Card.Title className="fw-bold">
                                    {cardName}
                                </Card.Title>

                                <Card.Subtitle className="fw-bold">
                                    Description:
                                </Card.Subtitle>
                                <Card.Text>{description}</Card.Text>

                                <Card.Subtitle className="fw-bold">
                                    Game:
                                </Card.Subtitle>
                                <Card.Text>{gameName}</Card.Text>

                                <Card.Subtitle className="fw-bold">
                                    Price:
                                </Card.Subtitle>
                                <Card.Text>Php {price}</Card.Text>

                                <Card.Subtitle className="fw-bold">
                                    Stocks:
                                </Card.Subtitle>
                                <Card.Text>{stock}</Card.Text>

                                {user.isAdmin && (
                                    <Fragment>
                                        <Card.Subtitle className="fw-bold">
                                            Is Active?
                                        </Card.Subtitle>
                                        <Card.Text>
                                            {isActive ? ` Yes` : ` No`}
                                        </Card.Text>
                                    </Fragment>
                                )}
                            </Col>
                        </Card.Body>
                    ) : (
                        <Form
                            className="text-dark bg-light p-3"
                            onSubmit={updateCard}
                        >
                            <Form.Group className="mb-3" controlId="cardName">
                                <Form.Label>Card Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter the card name"
                                    value={cardName}
                                    onChange={(event) => {
                                        setCardName(event.target.value);
                                    }}
                                    autoFocus={showEdit}
                                    required
                                />
                            </Form.Group>

                            <Form.Group
                                className="mb-3"
                                controlId="description"
                            >
                                <Form.Label>Description</Form.Label>
                                <Form.Control
                                    type="text"
                                    as="textarea"
                                    placeholder="Enter the description"
                                    value={description}
                                    onChange={(event) => {
                                        setDescription(event.target.value);
                                    }}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="gameName">
                                <Form.Label>Game:</Form.Label>
                                <Form.Select
                                    value={gameName}
                                    onChange={(event) => {
                                        setGameName(event.target.value);
                                    }}
                                    required
                                >
                                    <option disabled>Select game</option>
                                    <option value="Yu-Gi-Oh!">Yu-Gi-Oh!</option>
                                    <option value="Magic - The Gathering">
                                        Magic - The Gathering
                                    </option>
                                </Form.Select>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="price">
                                <Form.Label>Price:</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter the price"
                                    value={price}
                                    onChange={(event) => {
                                        setPrice(event.target.value);
                                    }}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="stock">
                                <Form.Label>Stock:</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter the stock"
                                    value={stock}
                                    onChange={(event) => {
                                        setStock(event.target.value);
                                    }}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="imgSrc">
                                <Form.Label>Image URL:</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter the image URL"
                                    value={imgSrc}
                                    onChange={(event) => {
                                        setImgSrc(event.target.value);
                                    }}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="isActive">
                                <Form.Label>Is Active?</Form.Label>{" "}
                                <Form.Check
                                    inline
                                    type="switch"
                                    aria-label="isActive"
                                    checked={isActive}
                                    onChange={() => {
                                        setIsActive((prev) => !prev);
                                    }}
                                ></Form.Check>
                            </Form.Group>
                        </Form>
                    )}
                    {/* {showQuantity && (
                        <Form>
                            <Form.Label>How many?</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Enter username or email"
                                required
                            />
                        </Form>
                    )} */}
                </Fragment>
            </Modal.Body>
            <Modal.Footer>
                {user.isAdmin ? (
                    <Fragment>
                        {!showEdit ? (
                            <Fragment>
                                <Button
                                    variant="primary"
                                    onClick={() => setShowEdit(true)}
                                >
                                    Edit
                                </Button>
                                <Button
                                    variant="secondary"
                                    onClick={props.onHide}
                                >
                                    Close
                                </Button>
                            </Fragment>
                        ) : (
                            <Fragment>
                                <Button variant="primary" onClick={updateCard}>
                                    Update
                                </Button>
                                <Button
                                    variant="secondary"
                                    onClick={() => setShowEdit(false)}
                                >
                                    Cancel
                                </Button>
                            </Fragment>
                        )}
                    </Fragment>
                ) : (
                    <Fragment>
                        <Button variant="primary" onClick={() => addToCart()}>
                            Add to Cart
                        </Button>
                    </Fragment>
                )}
            </Modal.Footer>
        </Modal>
    );
}
