import { Row, Col, Card } from "react-bootstrap";

export default function Highlights() {
    return (
        <Row className="w-100 my-4 px-5 align-items-center">
            <h3>Featured Cards</h3>
            <Col xs={12} md={4}>
                <Card className="align-items-center">
                    <Card.Body>
                        <img
                            src="https://drive.google.com/uc?export=view&id=1I99guVvXE8bX0I44oINtz-2iO_M31NQQ"
                            alt="card-back"
                        />
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="align-items-center">
                    <Card.Body>
                        <img
                            src="https://drive.google.com/uc?export=view&id=1I99guVvXE8bX0I44oINtz-2iO_M31NQQ"
                            alt="card-back"
                        />
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="align-items-center">
                    <Card.Body>
                        <img
                            src="https://drive.google.com/uc?export=view&id=1I99guVvXE8bX0I44oINtz-2iO_M31NQQ"
                            alt="card-back"
                        />
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}
