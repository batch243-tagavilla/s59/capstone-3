import { Fragment, useEffect, useState, useContext } from "react";
import { Row, Col, Card } from "react-bootstrap";
import CardView from "./CardView";
import UserContext from "../context/UserContext.js";
import { CardProvider } from "../context/CardContext.js";

export default function CardContainer(props) {
    const { user } = useContext(UserContext);
    const [modalShow, setModalShow] = useState(false);

    /* Card useState */
    const [cardId, setCardId] = useState("");
    const [cardName, setCardName] = useState("");
    const [description, setDescription] = useState("");
    const [gameName, setGameName] = useState("");
    const [price, setPrice] = useState("");
    const [stock, setStock] = useState(0);
    const [isActive, setIsActive] = useState(true);
    const [imgSrc, setImgSrc] = useState("");

    const openCardView = () => {
        setModalShow(true);
    };

    useEffect(() => {
        setCardId(props.cardProp._id);
        setCardName(props.cardProp.cardName);
        setDescription(props.cardProp.description);
        setGameName(props.cardProp.gameName);
        setPrice(props.cardProp.price);
        setStock(props.cardProp.stock);
        setIsActive(props.cardProp.isActive);
        setImgSrc(props.cardProp.imgSrc);
    }, []);

    return (
        <CardProvider
            value={{
                cardId,
                cardName,
                setCardName,
                description,
                setDescription,
                gameName,
                setGameName,
                price,
                setPrice,
                stock,
                setStock,
                isActive,
                setIsActive,
                imgSrc,
                setImgSrc,
            }}
        >
            <Card
                className="card-container p-3 col-12 col-md-6 col-lg-4"
                onClick={() => openCardView()}
            >
                <Card.Body className="p-0">
                    <Row>
                        <Col>
                            <img src={imgSrc} className="img-fluid" />
                        </Col>
                        <Col>
                            <Card.Title className="fw-bold">
                                {cardName}
                            </Card.Title>

                            <Card.Subtitle className="fw-bold">
                                Description:
                            </Card.Subtitle>
                            <Card.Text>{description}</Card.Text>

                            <Card.Subtitle className="fw-bold">
                                Game:
                            </Card.Subtitle>
                            <Card.Text>{gameName}</Card.Text>

                            <Card.Subtitle className="fw-bold">
                                Price:
                            </Card.Subtitle>
                            <Card.Text>Php {price}</Card.Text>

                            <Card.Subtitle className="fw-bold">
                                Stocks:
                            </Card.Subtitle>
                            <Card.Text>{stock}</Card.Text>

                            {user.isAdmin && (
                                <Fragment>
                                    <Card.Subtitle className="fw-bold">
                                        Is Active?
                                    </Card.Subtitle>
                                    <Card.Text>
                                        {isActive ? ` Yes` : ` No`}
                                    </Card.Text>
                                </Fragment>
                            )}
                        </Col>
                    </Row>
                </Card.Body>
            </Card>

            <CardView
                cardprop={props.cardProp}
                show={modalShow}
                onHide={() => {
                    setModalShow(false);
                }}
            />
        </CardProvider>
    );
}
